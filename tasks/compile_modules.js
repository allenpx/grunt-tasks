var fs = require('fs');
var _ = require('underscore')

module.exports = function (grunt) {

    grunt.registerMultiTask('compile_modules', 'Compile Less Modules', function() {
       // var po = require('node-po');
        var path = require('path');

        //make grunt know this task is async.
        var done = this.async();

        // Read the file and print its contents.
        var self = this;
        var data = this.data;

        var progress = {
            buildBaseDest:(data.buildBaseDest)?false:true,
            buildResponsiveDest:(data.buildResponsiveDest)?false:true,
            buildLegacyDest:(data.buildLegacyDest)?false:true,
            dest:false,
        }

       // console.log(data)
        fs.readFile(data.src, 'utf8', function(err, fileData) {

            if (err) throw err;

            var moduleFunctions = extractFunctions(fileData);

            if(data.buildBaseDest){
                fs.writeFile(data.buildBaseDest, createBuildFile(data.breakpoints, "base"), function(){
                    progress.buildBaseDest = true;
                    next();
                });
            }
            if(data.buildResponsiveDest){
                fs.writeFile(data.buildResponsiveDest, createBuildFile(data.breakpoints, "responsive"), function(){
                    progress.buildResponsiveDest = true;
                    next();
                });
            }
            if(data.buildLegacyDest){
                fs.writeFile(data.buildLegacyDest, createBuildFile(data.breakpoints, "legacy"), function(){
                    progress.buildLegacyDest = true;
                    next();
                });
            }

            fs.writeFile(data.dest, createFile(moduleFunctions), function(){
                progress.dest = true;
                next();
            });

        });

        function next(){

            if(
                progress.buildBaseDest && 
                progress.buildResponsiveDest && 
                progress.buildLegacyDest && 
                progress.dest
            ){
                done();
            }

        }


        function extractFunctions(string){
            var arr = string.match(data.regex);
            return _.uniq(arr);
        }

        function createFile(functionsArray){
            var stringFile = "";
            stringFile += '@import "modules.less";\n';
            _.each(data.breakpoints, function(breakpoint){

                stringFile += '\n.'+data.prefix+'breakpoint-'+breakpoint.name+'(){\n';

                stringFile += '\t@bp:"'+breakpoint.name+'";\n';
                _.each(functionsArray, function(functionName){
                    stringFile += '\t'+functionName+data.functionOverloads+');\n';
                });

                stringFile += '}\n\n';

            });

            return stringFile;

        }
        
        function createBuildFile(breakpoints, mode){

            /*
            .breakpoint-small();

            @media only screen and (min-width: 640px) {
                .breakpoint-medium();
            }
            @media only screen and (min-width: 800px) {
                .breakpoint-large();
            }
            @media only screen and (min-width: 1200px) {
                .breakpoint-xlarge();
            }
            @media only screen and (min-width: 1400px) {
                .breakpoint-xxlarge();
            }
            */

            var stringFile = "";

            if(mode == "base"){
                _.each(data.breakpoints, function(breakpoint){
                    if(breakpoint.size == 0){
                        stringFile += '\n.'+data.prefix+'breakpoint-'+breakpoint.name+'();\n';
                    }
                });
            }

            if(mode == "responsive"){
                _.each(data.breakpoints, function(breakpoint){
                    if(breakpoint.size != 0){
                        stringFile += "@media only screen and (min-width:"+breakpoint.size+"px) {\n"; 
                        stringFile += '\n.'+data.prefix+'breakpoint-'+breakpoint.name+'();\n';
                        stringFile += "}\n"; 
                    }
                });
            }

            if(mode == "legacy"){
                _.each(data.breakpoints, function(breakpoint){
                    if(breakpoint.size != 0){
                        stringFile += '\n.'+data.prefix+'breakpoint-'+breakpoint.name+'();\n';
                    }
                });
            }

            return stringFile;

        }

    });



};