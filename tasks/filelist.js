var fs = require('fs');
var _ = require('underscore');
_.templateSettings.interpolate = /\{\{(.+?)\}\}/g;
var path = require('path');

module.exports = function (grunt) {

    grunt.registerMultiTask('filelist', 'Create a list of files from a directory.', function() {

        var path = require('path');

        //make grunt know this task is async.
        var done = this.async();

        var defaultTransform = function(pathObj){

            var t = _.template(data.stringTransform)({
                filepath: path.normalize(pathObj.dir+'/'+pathObj.base),
                filename:  pathObj.name,
                extension: pathObj.ext
            });

            return t;

        };

        var self = this;
        var data = this.data;
        data.intro = data.intro || '';
        data.outro = data.outro || '';
        data.delimeter = data.delimeter || data.separator || '\n';
        if(_.isString(data.transform)){
            data.stringTransform = data.transform;
            data.transform = defaultTransform;
        }else{
            data.transform = data.transform || defaultTransform;
        }

        var files = grunt.file.expand(data.src);
        
        var newFiles = [];
        var transformed = [];

        _.each(files, function(filepath){
            newFiles.push( path.parse(path.relative( path.dirname(data.dest), filepath)) );
        },this);

        _.each(newFiles, function(pathObj){
            transformed.push(data.transform(pathObj));
        },this);

        var destString = data.intro + transformed.join(data.delimeter) + data.outro;

        fs.writeFile(data.dest, destString, function(){
            done();
        });


    });



};