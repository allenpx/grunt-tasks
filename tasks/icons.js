var fs = require('fs');
var _ = require('underscore');
var path = require('path');
var request = require('request');
var ProgressBar = require('progress');



module.exports = function (grunt) {

    grunt.registerMultiTask('icons', 'colorize svg icons', function() {

        var path = require('path');
        var done = this.async();

        var self = this;
        var data = this.data;

        data.defaultPrimary = data.defaultPrimary || "#111111";
        data.defaultSecondary = data.defaultSecondary || "#ff0000";
        data.namespace = data.namespace ||'icon';
        data.cssFormat = data.cssFormat || ".$namespace$(@name) when (@name = '$name$') { .svg-icon('$name$'); }\n";
        data.imports = data.imports || ['less-mixins/less-mixins.less'];
        data.additionalImports = data.additionalImports || [];
        data.imports = data.imports.concat(data.additionalImports);
        data.lessDest = 'icons.less' || data.lessDest;
        data.jsonDest = 'icons.json' || data.jsonDest;

        var primaryRegex = new RegExp(data.defaultPrimary, "gi");
        var secondaryRegex = new RegExp(data.defaultSecondary, "gi");

        var iconCSS = "";
        var nameFormatRegex = /\$name\$/g;
        var colorFormatRegex = /\$color\$/g;
        var namespaceFormatRegex = /\$namespace\$/g;
        var iconJSON = {icons:[]};

        var sourceIcons = grunt.file.expand(data.icons);

        sourceIcons.forEach(function(filePath){

            var file = grunt.file.read(filePath);
            var name = path.basename(filePath, ".svg").replace("icon_", "");
            var noun = name.replace(/-/g, ' ');
            if(name.indexOf('|') != -1){
                noun = name.split('|').pop();
                name = name.split('|')[0];
            }

            var newFile = "";
            var newFileName = name+'.svg';

            newFile = file.replace(primaryRegex, 'inherit').replace(secondaryRegex, 'currentColor');

            iconCSS += data.cssFormat.replace(nameFormatRegex, name).replace(namespaceFormatRegex, data.namespace);

            iconJSON.icons.push({
                name:name, 
                noun:noun,
                keywords:[]
            });

            grunt.file.write(data.iconsDest+'/'+newFileName, newFile);

        },this);

        var importContent = '';
        _.each(data.imports, function(importString){
            importContent += '@import "'+importString+'";\n\n';
        });

        iconCSS = importContent + iconCSS;

        grunt.file.write(data.lessDest, iconCSS);

        function finish(){
            grunt.file.write(data.jsonDest, JSON.stringify(iconJSON));
            done();
        }
        //lets make get some keywords

        var iconCount = iconJSON.icons.length;
        var iconsProcessed = 0;
        var bar = new ProgressBar('Getting Keywords [:bar]', { total: iconCount, width:20 });

        function processIcon(){
            var icon = iconJSON.icons[iconsProcessed];
            if(typeof icon != 'undefined'){
                request('http://words.bighugelabs.com/api/2/149cc658f4df20c0fe4d40cb16d7ae6a/'+icon.noun+'/json', function (error, response, body) {
                    if (!error && response.statusCode == 200) {
                        var json = JSON.parse(body);
                        _.each(json, function(type, key){
                            _.each(type, function(list){
                                icon.keywords = icon.keywords.concat(list);
                            });
                        });
                    }
                    icon.keywords.push(icon.noun);
                    icon.keywords = icon.keywords.concat(icon.noun.split(' '));
                    iconsProcessed += 1;
                    bar.tick();
                    processIcon();
                });
            }else{
                finish();
            }
        }

        processIcon();





    });



};